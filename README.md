A collection of handy git hooks.

## What are git hooks?

Git hooks are scripts that are executed before (`pre-`) or after (`post-`) a Git
event occurs or a command is triggered. What the script actually _does_ is
completely up to you!

Git events you can attach scripts to include:

+ applypatch-msg
+ commit-msg
+ post-applypatch
+ post-checkout
+ post-commit
+ post-merge
+ post-receive
+ post-rewrite
+ post-update
+ pre-applypatch
+ pre-auto-gc
+ pre-commit
+ pre-push
+ pre-rebase
+ pre-receive
+ prepare-commit-msg
+ update

Full details can be found in the [official Git docs](https://git-scm.com/docs/githooks).

## How to use these?

Copy the contents to the corresponding file (or create it) in your repositories `.git/hooks/` directory, and if needed, make it executable.
