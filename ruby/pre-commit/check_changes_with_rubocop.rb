#!/usr/bin/env ruby

# Grab a list of all changed files, and trim the first 4 characters (git status)
#
# TODO: This could be smarter and only check currently staged files
# TODO: How does this handle deleted or moved files? Not well, I bet!
#
changed_files = `git status -s | cut -c4-`

# Turn the \n delimited list of files into an array, and drop any that aren't
#   Ruby (and thus will cause Rubocop errors)
#
file_list = changed_files.split("\n").select{ |file| file.end_with?(".rb") }

# Pass file_list to rubocop, which accepts a space-delimited list of multiple
#   files to process
#
if file_list.empty?
  exit
else
  unless system("rubocop #{file_list.join(" ")}")
    puts "\n\n\033[1;4;31m** Aborting commit **\033[0m"
    exit 1
  end
end
