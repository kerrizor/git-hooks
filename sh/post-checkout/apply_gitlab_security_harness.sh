#!/bin/sh

# This is a script specific to GitLab, so likely not useful to anyone outside
#   GitLab contributors, however it can serve as a good starting point to seeing
#   how we can run a script when changing branches.
#
# --------------------------------------------------
#

# Triggers scripts/security-harness when moving between branches. It runs the
#   script when moving from a branch whose name begins with security- and the
#   target does not, or vice versa. If you're moving between 2 branches that
#   BOTH start with security-, or neither of them do, it does nothing.
#
# It'd be lovely if security-harness set an ENV flag of some sort and we could
#   detect that, but that's an iteration ^_^
#

# post-checkout passes 3 parameter in, but we only care about the 3rd right now.
#
# prevHEAD=$1
# newHEAD=$2
checkoutType=$3

# 1 is a branch checkout; 0 is a file checkout
#
if [ $checkoutType == 1 ]
then
  # I know we could do multiple assignment with IFS but I couldn't get it
  #   working. Leaving it here as a breadcrumb for someone with more knowledge
  #   or determination
  #
  # IFS=$'\v' read previousBranch currentBranch <<< branchNames

  gitReflog=`git reflog`
  previousBranch=`echo $gitReflog | awk 'NR==1{ print $6; exit }'`
  currentBranch=`echo $gitReflog | awk 'NR==1{ print $8; exit }'`

  if [[ $currentBranch = security-* && $previousBranch != security-* ]]
  then
    ./scripts/security-harness
  elif [[ $currentBranch != security-* && $previousBranch = security-* ]]
  then
    ./scripts/security-harness
  fi
fi
