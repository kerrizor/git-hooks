#!/bin/sh

git status -s | cut -c4- | xargs rubocop

if [ $? -eq 1 ]
then
  echo "\n\n\033[1;4;31m** Aborting commit **\033[0m"
  exit 1
fi
